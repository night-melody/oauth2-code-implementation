package com.mbw.pojo;

public enum Currency {
    USD,
    GBP,
    EUR;
}
