package com.mbw.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@TableName("product")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Product {
    @TableId(type= IdType.ASSIGN_ID)
    private Long id;
    @TableField("name")
    private String name;
    @TableField("price")
    private double price;
    @TableField("currency")
    private Currency currency;
    @TableField(exist = false)
    private Integer num;
}
