package com.mbw.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("oauth_client_details")
public class OAuth2Client implements Serializable {
	@TableId(type = IdType.ASSIGN_ID, value = "id")
	private Long id;
	@TableField("clientId")
	private String clientId;
	@TableField(value = "resourceIds")
	private String resourceIds;
	@TableField(value = "authorities")
	private String authorities;
	@TableField("clientSecret")
	private String clientSecret;
	@TableField(value = "scope")
	private String scope;
	@TableField(value = "authorizedGrantTypes")
	private String authorizedGrantTypes;
	@TableField("webServerRedirectUri")
	private String webServerRedirectUri;
	@TableField("accessTokenValidity")
	private Integer accessTokenValidity;
	@TableField("refreshTokenValidity")
	private Integer refreshTokenValidity;
	@TableField("additionalInformation")
	private String additionalInformation;
	@TableField("autoApprove")
	private Boolean autoApprove;
}
