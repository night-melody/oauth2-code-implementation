package com.mbw.vo;

import java.time.LocalDateTime;

public class SmsCode {
    private String text;
    private LocalDateTime expireTime;
    private String mobile;

    public String getMobile() {
        return mobile;
    }

    public String getText() {
        return text;
    }

    public SmsCode(String text, String mobile, int expireTimeAfterSeconds) {
        this.text = text;
        this.mobile = mobile;
        //设置过期时间
        this.expireTime = LocalDateTime.now().plusSeconds(expireTimeAfterSeconds);
    }

    //是否过期
    public boolean isExpired() {
        return LocalDateTime.now().isAfter(expireTime);
    }
}
