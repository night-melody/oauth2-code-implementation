package com.mbw.vo;

import java.time.LocalDateTime;

public class CaptchaImageVO {
	private String text;
	private LocalDateTime expireTime;

	public String getText() {
		return text;
	}

	public CaptchaImageVO(String text, int expireTimeAfterSeconds) {
		this.text = text;
		//设置过期时间
		this.expireTime = LocalDateTime.now().plusSeconds(expireTimeAfterSeconds);
	}
	//是否过期
	public boolean isExpired(){
		return LocalDateTime.now().isAfter(expireTime);
	}
}