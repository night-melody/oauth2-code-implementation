package com.mbw.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

	@GetMapping("/xiao")
	public String xiao(){return "纳西妲我抽爆！";}
	@GetMapping("/giao")
	public String giao(){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String name = authentication.getName();
		return "giao,"+name;
	}
	@GetMapping("/a")
	public String getEndpointA(){
		return "a";
	}
	@GetMapping("/a/b")
	public String getEndpointAB(){
		return "ab";
	}

	@GetMapping("/product/{code}")
	public String productCode(@PathVariable String code){
		return code;
	}
}
