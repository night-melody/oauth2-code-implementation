package com.mbw.controller;

import com.mbw.common.utils.Res;
import com.mbw.pojo.OAuth2Client;
import com.mbw.service.OAuth2ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/oauth2/client")
public class OAuth2ClientController {
	@Autowired
	private OAuth2ClientService oAuth2ClientService;

	@PostMapping("query/clientId")
	public Res queryOauthClientByClientId(String clientId){
		OAuth2Client oAuth2Client = oAuth2ClientService.queryClientByClientId(clientId);
		return Res.success(oAuth2Client);
	}

	@PostMapping("create")
	public Res createOAuthClient(@RequestBody OAuth2Client oAuth2Client){
		OAuth2Client oAuth2ClientCreated = oAuth2ClientService.createOAuth2Client(oAuth2Client);
		return Res.success(oAuth2ClientCreated);
	}
}
