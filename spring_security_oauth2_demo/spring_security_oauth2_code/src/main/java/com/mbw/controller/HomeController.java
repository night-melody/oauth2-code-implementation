package com.mbw.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.oauth2.client.OAuth2AuthorizationContext;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
public class HomeController {
	@GetMapping("/")
	public String home(OAuth2AuthenticationToken token)
	{
		return "home";
	}

	@GetMapping("/error")
	public String error(){
		return "error";
	}

	@GetMapping("/toLogin")
	public String login(){return "login";}


}
