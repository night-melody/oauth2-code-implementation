package com.mbw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.security.Principal;


@RestController
@RequestMapping("/api")
public class HelloController {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/hello")
    public String hello(){
        return "hello world";
    }
    @GetMapping("/yidou")
    public String xiao(){return "一斗我抽爆！";}
    @GetMapping("/giao")
    public String giao(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String name = authentication.getName();
        return "giao,"+name;
    }
    @PostMapping("/a")
    public String postEndpointA(){
        return "a";
    }
    @GetMapping("/a")
    public String getEndpointA(){
        return "a";
    }
    @GetMapping("/member")
    public Principal user(Principal member) {
        return member;
    }
}
