package com.mbw.controller;

import cn.hutool.core.util.RandomUtil;
import com.mbw.common.utils.Result;
import com.mbw.mapper.UserMapper;
import com.mbw.pojo.User;
import com.mbw.service.SmsCodeSendService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Slf4j
@RestController
public class SmsController {
    @Resource
    private UserMapper userDetailsMapper;
    @Resource
    private SmsCodeSendService smsCodeSendService;

    @RequestMapping("/smsCode")
    public Result getSmsCaptcha(@RequestParam String mobile) {
        User user = userDetailsMapper.findByMobile(mobile);
        if (user == null) {
            return Result.error().message("你输入的手机号未注册");
        }
       smsCodeSendService.sendSmsCode(mobile, RandomUtil.randomNumbers(4));
        return Result.ok().message("发送验证码成功");
    }
}
