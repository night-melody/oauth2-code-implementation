package com.mbw.security.service;

import com.mbw.mapper.OAuth2ClientMapper;
import com.mbw.pojo.OAuth2Client;
import com.mbw.security.dto.OAuth2ClientDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

@Service
public class ClientDetailsServiceImpl implements ClientDetailsService {

	@Autowired
	private OAuth2ClientMapper oAuth2ClientMapper;

	@Override
	public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
		OAuth2Client oAuth2Client = oAuth2ClientMapper.queryByClientId(clientId);
		return new OAuth2ClientDto(oAuth2Client);
	}
}
