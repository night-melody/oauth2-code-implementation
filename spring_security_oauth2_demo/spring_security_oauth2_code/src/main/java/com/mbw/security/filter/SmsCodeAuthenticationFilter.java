package com.mbw.security.filter;

import com.mbw.security.authentication.SmsCodeAuthenticationToken;
import org.springframework.lang.Nullable;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//仿造UsernamePasswordAuthenticationFilter写一个专门为手机短信验证登陆的过滤器
public class SmsCodeAuthenticationFilter extends AbstractAuthenticationProcessingFilter {
	//报错：参数错误
	public static final String SPRING_SECURITY_FORM_MOBILE_KEY = "mobile";
	private String mobileParameter = SPRING_SECURITY_FORM_MOBILE_KEY;
	private boolean postOnly = true;

	//必须要构造器，构造器参数为请求路径和请求方法
	public SmsCodeAuthenticationFilter() {
		super(new AntPathRequestMatcher("/smslogin", "POST"));
	}

	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
		if (this.postOnly && !request.getMethod().equals("POST")) {
			throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
		} else {
			//从request中获取Mobile
			String mobile = this.obtainMobile(request);
			if (mobile == null) {
				mobile = "";
			}
			mobile = mobile.trim();
			//此时调用的是第一个参数的authentication,即认证前，principal存放的是mobile
			SmsCodeAuthenticationToken authentication = new SmsCodeAuthenticationToken(mobile);
			setDetails(request, authentication);
			//认证
			return this.getAuthenticationManager().authenticate(authentication);
		}
	}

	//获取参数
	@Nullable
	protected String obtainMobile(HttpServletRequest request) {
		return request.getParameter(this.mobileParameter);
	}


	protected void setDetails(HttpServletRequest request, SmsCodeAuthenticationToken authRequest) {
		authRequest.setDetails(this.authenticationDetailsSource.buildDetails(request));
	}
}
