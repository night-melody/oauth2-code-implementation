package com.mbw.security.dto;

import cn.hutool.core.text.CharSequenceUtil;
import com.mbw.pojo.OAuth2Client;
import lombok.Data;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
@ToString
public class OAuth2ClientDto implements ClientDetails {

	private OAuth2Client oAuth2Client;

	@Override
	public String getClientId() {
		return oAuth2Client.getClientId();
	}

	@Override
	public Set<String> getResourceIds() {
		String resourceIds = oAuth2Client.getResourceIds();
		if(CharSequenceUtil.isNotBlank(resourceIds)){
			String[] resourceCollection = resourceIds.split(",");
			return new HashSet<>(Arrays.asList(resourceCollection));
		}
		return Collections.emptySet();
	}

	@Override
	public boolean isSecretRequired() {
		return true;
	}

	@Override
	public String getClientSecret() {
		return oAuth2Client.getClientSecret();
	}

	@Override
	public boolean isScoped() {
		return true;
	}

	@Override
	public Set<String> getScope() {
		String scopes = oAuth2Client.getScope();
		if(CharSequenceUtil.isNotBlank(scopes)){
			String[] scopeCollection = scopes.split(",");
			return new HashSet<>(Arrays.asList(scopeCollection));
		}
		return Collections.emptySet();
	}

	@Override
	public Set<String> getAuthorizedGrantTypes() {
		String authorizedGrantTypes = oAuth2Client.getAuthorizedGrantTypes();
		if(CharSequenceUtil.isNotBlank(authorizedGrantTypes)){
			String[] grantTypes = authorizedGrantTypes.split(",");
			return new HashSet<>(Arrays.asList(grantTypes));
		}
		return Collections.emptySet();
	}

	@Override
	public Set<String> getRegisteredRedirectUri() {
		String webServerRedirectUri = oAuth2Client.getWebServerRedirectUri();
		if(CharSequenceUtil.isNotBlank(webServerRedirectUri)){
			HashSet<String> redirectUris = new HashSet<>();
			redirectUris.add(webServerRedirectUri);
			return redirectUris;
		}
		return Collections.emptySet();
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		String authorities = oAuth2Client.getAuthorities();
		List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
		if(CharSequenceUtil.isNotBlank(authorities)){
			String[] grantAuthorities = authorities.split(",");
			for (String grantAuthority : grantAuthorities) {
				SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(grantAuthority);
				grantedAuthorityList.add(simpleGrantedAuthority);
			}
			return grantedAuthorityList;
		}
		return Collections.emptyList();
	}

	@Override
	public Integer getAccessTokenValiditySeconds() {
		return oAuth2Client.getAccessTokenValidity();
	}

	@Override
	public Integer getRefreshTokenValiditySeconds() {
		return oAuth2Client.getRefreshTokenValidity();
	}

	@Override
	public boolean isAutoApprove(String s) {
		return oAuth2Client.getAutoApprove();
	}

	@Override
	public Map<String, Object> getAdditionalInformation() {
		String additionalInformation = oAuth2Client.getAdditionalInformation();
		if(CharSequenceUtil.isNotBlank(additionalInformation)){
			HashMap<String, Object> information = new HashMap<>();
			information.put("额外信息",additionalInformation);
			return information;
		}
		return Collections.emptyMap();
	}

	public OAuth2ClientDto(OAuth2Client oAuth2Client) {
		this.oAuth2Client = oAuth2Client;
	}
}
