package com.mbw.security.service;

import cn.hutool.core.util.StrUtil;
import com.mbw.mapper.AuthorityMapper;
import com.mbw.mapper.UserMapper;
import com.mbw.pojo.Authority;
import com.mbw.pojo.Role;
import com.mbw.pojo.User;
import com.mbw.security.dto.JwtUserDto;
import com.mbw.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RoleService roleService;

    @Autowired
    private AuthorityMapper authorityMapper;

    @Override
    public JwtUserDto loadUserByUsername(String username) throws UsernameNotFoundException {
        // 根据用户名获取用户
        User user = userMapper.queryUserByUsername(username);
        if (user == null ){
            throw new BadCredentialsException("用户名或密码错误");
        }
        List<Role> roles = roleService.loadRolesByUsername(username);
        Set<String> roleInfos = roles.stream().map(Role::getRoleName).collect(Collectors.toSet());
        List<Authority> authorities = authorityMapper.loadPermissionByRoleCode(roleInfos);
        List<String> authorityNames = authorities.stream().map(Authority::getAuthorityName).filter(StrUtil::isNotEmpty).collect(Collectors.toList());
        authorityNames.addAll(roleInfos.stream().map(roleName->"ROLE_"+roleName).collect(Collectors.toList()));
        JwtUserDto jwtUserDto = new JwtUserDto(user, new HashSet<>(roles), authorityNames);
        jwtUserDto.setLoginTime(System.currentTimeMillis());
        return jwtUserDto;
    }
}
