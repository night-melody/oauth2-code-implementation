package com.mbw.security.token;

		import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;

public class DefaultOAuth2AccessTokenEx extends DefaultOAuth2AccessToken {
	private DefaultOAuth2RefreshTokenEx refreshToken;

	public DefaultOAuth2AccessTokenEx() {
		super((String) null);
	}

	@Override
	public void setValue(String value) {
		super.setValue(value);
	}

	@Override
	public DefaultOAuth2RefreshTokenEx getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(DefaultOAuth2RefreshTokenEx refreshToken) {
		this.refreshToken = refreshToken;
	}
}
