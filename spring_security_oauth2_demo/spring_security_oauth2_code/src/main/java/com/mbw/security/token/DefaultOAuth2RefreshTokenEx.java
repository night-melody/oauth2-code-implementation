package com.mbw.security.token;

import lombok.Data;
import org.springframework.security.oauth2.common.DefaultOAuth2RefreshToken;
import org.springframework.security.oauth2.common.ExpiringOAuth2RefreshToken;

import java.util.Date;

@Data
public class DefaultOAuth2RefreshTokenEx extends DefaultOAuth2RefreshToken implements ExpiringOAuth2RefreshToken {
	private Date expiration;
	private String value;

	public DefaultOAuth2RefreshTokenEx() {
		super(null);
	}
}
