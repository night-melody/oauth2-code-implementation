package com.mbw.security.client.gitee;

import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.stereotype.Component;

@Component
public class GiteeClient {

	public ClientRegistration clientRegistration(){
		return ClientRegistration.withRegistrationId("gitee")  //起个名字,代表client，如clientId和clientSecret
				.clientId("your clientId")  //此处要换成你在gitee上创建应用得到的
				.clientSecret("your clientSecret") //此处要换成你在gitee上创建应用得到的
				.scope(new String[]{"user_info"})    //读取用户权限，参见你gitee上创建应用时的授权勾选
				.authorizationUri("https://gitee.com/oauth/authorize")   //这要看gitee的api，是user认证以及client认证获取授权码的地址
				.tokenUri("https://gitee.com/oauth/token") //这要看gitee的api，是client得到授权码后去换token的gitee地址
				.userInfoUri("https://gitee.com/api/v5/user") //资源服务器api地址-也是client用access-token去获取用户user详情的“用户详情资源服务器地址”-这里也是gitee】】
				.userNameAttributeName("id")
				.clientName("gitee")  //为我们的应用client起了个名字
				.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)  //注是授权码模式
				.redirectUriTemplate("{baseUrl}/{action}/oauth2/code/{registrationId}")  //本应用配置的gitee发回授权码的地址
				.build();
	}

	public ClientRegistration githubClient(){
		return CommonOAuth2Provider.GITHUB
				.getBuilder("github")
				.clientId("your clientId")
				.clientSecret("your clientSecret")
				.build();
	}
}
