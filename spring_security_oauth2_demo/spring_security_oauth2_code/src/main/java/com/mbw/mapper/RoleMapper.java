package com.mbw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mbw.pojo.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RoleMapper extends BaseMapper<Role> {
    List<Role> queryAllRoleByRoleName();
    /**
     * 根据用户名获取角色
     *
     * @param username
     * @return List<SysRole>
     */
    List<Role> loadRolesByUsername(@Param("username") String username);

}
