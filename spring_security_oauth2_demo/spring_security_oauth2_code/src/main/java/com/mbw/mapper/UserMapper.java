package com.mbw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mbw.pojo.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {
   User queryUserByUsername(String username);
   User checkUsernameUnique(String userName);
   User checkPhoneUnique(String phone);
   User findByMobile(String mobile);
}
