package com.mbw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mbw.pojo.Authority;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

@Mapper
public interface AuthorityMapper extends BaseMapper<Authority> {
    /**
     * 通过角色名称list查询菜单权限
     */
    List<Authority> loadPermissionByRoleCode(@Param("roleInfos") Set<String> roleInfos);
}
