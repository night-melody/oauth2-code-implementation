package com.mbw.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mbw.pojo.OAuth2Client;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OAuth2ClientMapper extends BaseMapper<OAuth2Client> {
	OAuth2Client queryByClientId(String clientId);
	OAuth2Client queryClientUnique(String clientId);
}
