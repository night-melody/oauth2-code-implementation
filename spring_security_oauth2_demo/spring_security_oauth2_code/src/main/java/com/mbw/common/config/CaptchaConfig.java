package com.mbw.common.config;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;
import java.util.Properties;

//验证码配置设置
@Configuration
@PropertySource("classpath:kaptcha.properties")
public class CaptchaConfig {
    @Bean
    public DefaultKaptcha getKaptchaBean() throws IOException {
        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        Properties p = new Properties();
        //将配置文件的配置加载
        p.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("kaptcha.properties"));
        //set进defaultKaptcha类
        defaultKaptcha.setConfig(new Config(p));
        return defaultKaptcha;
    }
}
