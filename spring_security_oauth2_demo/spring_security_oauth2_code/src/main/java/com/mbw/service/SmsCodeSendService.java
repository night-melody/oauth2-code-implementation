package com.mbw.service;

import com.mbw.common.utils.UserConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;

@Service
@Slf4j
public class SmsCodeSendService {
	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	public boolean sendSmsCode(String mobile, String code) {
		//因为这里是示例所以就没有真正的使用第三方发送短信平台。
		String sendCode = String.format("你好你的验证码%s，请勿泄露他人。", code);
		log.info("向手机号" + mobile + "发送的短信为:" + sendCode);
		stringRedisTemplate.opsForValue().set(mobile + "_" + UserConstants.SMS_REDIS_KEY, code, Duration.ofMinutes(5));
		return true;
	}

}
