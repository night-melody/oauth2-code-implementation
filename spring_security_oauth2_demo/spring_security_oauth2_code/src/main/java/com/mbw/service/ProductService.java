package com.mbw.service;

import com.mbw.pojo.Product;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
	public int getNum(Product product){
		return product.getNum();
	}
}
